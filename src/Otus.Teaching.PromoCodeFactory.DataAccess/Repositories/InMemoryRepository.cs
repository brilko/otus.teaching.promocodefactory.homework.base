﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<IEnumerable<T>> GetArrangeByIdAsync(IEnumerable<Guid> ids)
        {
            return Task.FromResult(Data.Where(el => ids.Contains(el.Id)));
        }

        public async Task<bool> DeleteByIdAsync(Guid id)
        {
            bool isDeleted = false;
            await Task.Run(() => 
            {
                var entity = Data.Where(el => el.Id == id).FirstOrDefault();
                if (entity == null)
                {
                    isDeleted = false;
                    return;
                }
                Data = Data.Where(el => el.Id != id);
                isDeleted = true;
            }); 
            return isDeleted;
        }

        public async Task<Guid> CreateAsync(T entity)
        {
            var id = Guid.NewGuid();
            await Task.Run(() => 
            {
                entity.Id = id;
                Data = Data.Append(entity);
            });
            return id;
        }

        public async Task<bool> UpdateAsync(T entity)
        {
            var isUpdated = false;
            await Task.Run(() =>
            {
                if (!Data.Any(el => el.Id == entity.Id))
                {
                    isUpdated = false;
                    return;
                }
                Data = Data.Where(el => el.Id != entity.Id).Append(entity);
                isUpdated = true;
            });
            return isUpdated;
        }
    }
}