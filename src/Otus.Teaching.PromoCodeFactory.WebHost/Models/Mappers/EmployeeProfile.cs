﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models.Mappers
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<EmployeeCreateOrUpdate, Employee>().
                ForMember(dest => dest.Id, opt => opt.MapFrom(s => Guid.Empty)).
                ForMember(dest => dest.Roles, opt => opt.Ignore());
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Employee, EmployeeResponse>().
                ForMember(dest => dest.Roles, opt => opt.Ignore()).
                AfterMap((src, dst, context) =>
                {
                    dst.Roles = context.Mapper.Map<List<RoleItemResponse>>(src.Roles);
                });
        }
    }
}
