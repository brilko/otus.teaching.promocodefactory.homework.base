﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> employeeRepository;
        private readonly IRepository<Role> roleRepository;
        private readonly IMapper mapper;

        public EmployeesController(
            IRepository<Employee> employeeRepository,
            IRepository<Role> roleRepository,
            IMapper mapper)
        {
            this.employeeRepository = employeeRepository;
            this.roleRepository = roleRepository;
            this.mapper = mapper;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await employeeRepository.GetAllAsync();

            var employeesModelList = mapper.Map<List<EmployeeShortResponse>>(employees);

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = mapper.Map<EmployeeResponse>(employee);

            return employeeModel;
        }


        /// <summary>
        /// Создать нового сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult<Guid>> CreateEmployeeAsync(EmployeeCreateOrUpdate employeeRequest)
        {
            var employee = mapper.Map<Employee>(employeeRequest);
            employee.Roles = (await roleRepository.GetArrangeByIdAsync(employeeRequest.RolesIds)).ToList();
            var guid = await employeeRepository.CreateAsync(employee);
            return Created(guid.ToString(), employee);
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            if (await employeeRepository.DeleteByIdAsync(id))
                return Ok();
            return NotFound();
        }

        /// <summary>
        /// Обновить данные сотрудника
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult> UpdateEmployeeAsync(Guid id, EmployeeCreateOrUpdate employeeRequest)
        {
            var employee = mapper.Map<Employee>(employeeRequest);
            employee.Id = id;
            employee.Roles = (await roleRepository.GetArrangeByIdAsync(employeeRequest.RolesIds)).ToList();
            if (await employeeRepository.UpdateAsync(employee))
                return Ok();
            return NotFound();
        }
    }
}